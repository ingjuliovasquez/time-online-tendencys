const express = require('express');
const router = express.Router();
const axios = require('axios');
const indexController = require('../controllers/indexController')



router.get('/', indexController.getContador);

module.exports = router;