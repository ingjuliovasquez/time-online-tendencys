const express = require('express');
const http = require('http');
const socketIO = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);
const axios = require('axios')


const indexRouter = require('./routes/index')
app.use('/', indexRouter)

let contador = 0;
function incrementarContador() {
    contador++;
    io.emit('actualizarContador', contador);
}

app.get('/generarGuia', async (req, res) => {
    const dataRaw = {
        "origin": {
            "name": "oscar mx",
            "company": "oskys factory",
            "email": "osgosf8@gmail.com",
            "phone": "8116300800",
            "street": "av vasconcelos",
            "number": "1400",
            "district": "mirasierra",
            "city": "Monterrey",
            "state": "NL",
            "country": "MX",
            "postalCode": "66236",
            "reference": ""
        },
        "destination": {
            "name": "oscar",
            "company": "empresa",
            "email": "osgosf8@gmail.com",
            "phone": "8116300800",
            "street": "av vasconcelos",
            "number": "1400",
            "district": "palo blanco",
            "city": "monterrey",
            "state": "NL",
            "country": "MX",
            "postalCode": "66240",
            "reference": ""
        },
        "packages": [
            {
                "content": "camisetas rojas",
                "amount": 2,
                "type": "box",
                "dimensions": {
                    "length": 2,
                    "width": 5,
                    "height": 5
                },
                "weight": 63,
                "insurance": 0,
                "declaredValue": 400,
                "weightUnit": "KG",
                "lengthUnit": "CM"
            },
            {
                "content": "camisetas rojas",
                "amount": 2,
                "type": "box",
                "dimensions": {
                    "length": 1,
                    "width": 17,
                    "height": 2
                },
                "weight": 5,
                "insurance": 400,
                "declaredValue": 400,
                "weightUnit": "KG",
                "lengthUnit": "CM"
            }
        ],
        "shipment": {
            "carrier": "DHL",
            "service": "express",
            "type": 1
        },
        "settings": {
            "printFormat": "PDF",
            "printSize": "STOCK_4X6",
            "comments": "comentarios de el envío"
        }
    }
    try {
        const axiosConfig = {
            headers: {
                Authorization: `Bearer e4df5451c3ec3cdf3e08d4747de6f12ea0eb1ef162b5cae0227028ba030672c2`,
                'Content-Type': 'application/json'
            },
        };
        await axios.post('https://api-test.envia.com/ship/generate', dataRaw, axiosConfig);
        incrementarContador();
        res.status(200).send('Solicitud POST exitosa');
    } catch (error) {
        console.error('Error en la solicitud POST:', error.message);
        res.status(500).send('Error en la solicitud POST');
    }
})

io.on('connection', (socket) => {
    socket.emit('actualizarContador', contador);
    socket.on('incrementarContador', () => {
        incrementarContador();
    });
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
    console.log(`Servidor escuchando en el puerto ${PORT}`);
});
